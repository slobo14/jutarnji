﻿using Jutarnji.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Jutarnji.Controllers
{
    public class Entries : ViewComponent
    {
        private IEntriesProvider _entriesProvider;
        public Entries(IEntriesProvider _provider)
        {
            _entriesProvider = _provider;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var entries = _entriesProvider.getAllEntries();
            if (entries != null)
                return View("Entries.cshtml", entries);
            else
                return (IViewComponentResult)new EmptyResult();
        }
    }
}
