﻿using System;
using System.Collections.Generic;
using Jutarnji.Interfaces;
using Jutarnji.Models;
using System.Net.Http;
using System.Xml.Linq;
using System.Linq;
using System.Globalization;

namespace Jutarnji.Provider
{
    public class EntriesProvider : IEntriesProvider
    {
        private Func<XElement, XNamespace, string, string> getLink = null;

        public EntriesProvider()
        {
            if (getLink == null)
            {
                getLink = (XElement item, XNamespace namespaceXml, string type) =>
                {
                    var links = (from link in item.Descendants(namespaceXml + "link")
                                 where link.Attribute("rel").Value == type
                                 select link.Attribute("href").Value);

                    return (links.Count() > 0) ? links.ToList()[0] : "#";
                };
            }
        }

        public IEnumerable<Entry> getAllEntries()
        {
            var downloader = new HttpClient();
            var uri = new Uri("http://www.jutarnji.hr/rss/vijesti/", UriKind.Absolute);
            var stream = downloader.GetStreamAsync(uri).Result;
            var doc = XDocument.Load(stream);
            var namespaceXml = doc.Root.Name.Namespace;

            var entries = from item in doc.Descendants(namespaceXml + "entry")
                          select new Entry()
                          {
                              Title      = item.Element(namespaceXml + "title").Value,
                              Link       = getLink(item, namespaceXml, "alternate"),
                              Published  = DateTime.ParseExact(item.Element(namespaceXml + "published").Value, "yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture),
                              Summary    = item.Element(namespaceXml + "summary").Value,
                              Categories = from cat in item.Descendants(namespaceXml + "category") select cat.Attribute("term").Value
                          };
            return entries;
        }

        public IEnumerable<Entry> getEntryByPage(int page)
        {
            var entries = getAllEntries();

            int rangeStart = 0;
            int rangeLength = 0;

            int pageSize = 12;
            int pageNumber = page;

            if ((pageNumber * pageSize) > entries.Count())
                return null;

            if (((pageNumber * pageSize) + pageSize) > entries.Count())
            {
                rangeLength = entries.Count() % pageSize;
                rangeStart = entries.Count() - rangeLength;
            }
            else
            {
                rangeStart = (pageNumber * pageSize);
                rangeLength = pageSize;
            }

            entries = entries.ToList().GetRange(rangeStart, rangeLength);

            return entries;
        }
    }
}
