﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jutarnji.Models
{
    public class Entry
    {
        public string Title { get; set; }
        public string Link { get; set; }
        
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Published { get; set; }
        public string Summary { get; set; }
        public IEnumerable<string> Categories { get; set; }
    }
}
