﻿using Jutarnji.Models;
using System.Collections.Generic;

namespace Jutarnji.Interfaces
{
    public interface IEntriesProvider
    {
        IEnumerable<Entry> getAllEntries();
        IEnumerable<Entry> getEntryByPage(int page);
    }
}
